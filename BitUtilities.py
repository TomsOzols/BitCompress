def count_file_true_bits(file):
    filebytes = bytearray(file.read())
    return count_true_bits(filebytes)

def count_true_bits(filebytes):
    counter = 0
    for byte in filebytes:
        for bitnumber in range(8):
            bit = (byte >> bitnumber) & 1
            if bit:
                counter = counter + 1

    return counter

def incrementIfValid(item, currentIndex, bit):
    number, hits = item
    if currentIndex % number == 0 and bit == 1:
        hitIncrement = hits + 1
        return (number, hitIncrement)
    else:
        return (number, hits)

def count_true_bit_occurences(upto, filebytes):
    howmany = range(1, upto + 1)
    counters = map(lambda item: (item, 0), howmany)
    listCounters = list(counters)
    for index, byte in enumerate(filebytes):
        for bitnumber in range(8):
            current_byte = index + 1
            current_bit = bitnumber + 1
            current_bit_index = (current_byte * 8) - (8 - current_bit)
            bit = (byte >> bitnumber) & 1

            listCounters = list(map(lambda item: incrementIfValid(item, current_bit_index, bit), listCounters))

    return listCounters
    