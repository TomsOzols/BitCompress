def compare_byte_arrays(bytes1, bytes2):
    array_length = len(bytes1)
    if not array_length == len(bytes2):
        raise Exception('Array lengths different')
    for iterator in range(array_length):
        if not bytes1[iterator] == bytes2[iterator]:
            return False
    return True

