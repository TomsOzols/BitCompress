# from RandomFileGenerator import generate_one_byte_file

# BINARY_PREFIX_MIB_BYTES = 1048576
# generatefile(BINARY_PREFIX_MIB_BYTES)
# generate_one_byte_file()

# from FileUtilities import compare_byte_arrays

# with open('TestFiles/inputFile', 'rb') as inputfile:
#     with open('TestFiles/inputFileCopy', 'rb') as inputFile2:
#         FILE_BYTES1 = inputfile.read()
#         FILE_BYTES2 = inputFile2.read()
#         IS_FILES_EQUAL = compare_byte_arrays(FILE_BYTES1, FILE_BYTES2)
#         print(IS_FILES_EQUAL)

# from BitUtilities import count_true_bits
# with open('TestFiles/oneByte', 'rb') as inputfile:
#     TRUE_BIT_COUNT = count_file_true_bits(inputfile)
#     print(TRUE_BIT_COUNT)

import sys
from BitUtilities import count_true_bit_occurences, count_true_bits

# with open('TestFiles/inputFile', 'rb') as inputFile:
with open('TestFiles/inputFile', 'rb') as inputFile:
    fileBytes = bytearray(inputFile.read())
    firstBytes = fileBytes[:32]

    try:
        counters = count_true_bit_occurences(256, fileBytes)
        truebits = count_true_bits(fileBytes)

        with open('OutputFiles/result.txt', 'w+') as outPutFile:
            outPutFile.write(str(truebits))
            outPutFile.write('\n')
            for counter in counters:
                number, hits = counter
                outPutFile.write(f'{number}:::{hits}')
                outPutFile.write('\n')
        # print('+++++++++++++++++++++++++++++++')
        # print('First 256 bits')
        # print(firstBytes)
        # print('+++++++++++++++++++++++++++++++')
        # print('+++++++++++++++++++++++++++++++')
        # print('True bit count')
        # print(truebits)
        # print('+++++++++++++++++++++++++++++++')
        # print('+++++++++++++++++++++++++++++++')
        # for number, hits in counters:
        #     print(f'{number}:::{hits}')

    except Exception as excep:
        print(excep.args)
