from os import urandom

def generate_random_byte_file(bytelength):
    randombytes = bytearray(urandom(bytelength))
    write_bytes_to_file('inputFile4', randombytes)

def generate_one_byte_file():
    onebyte = bytearray([251])
    write_bytes_to_file('oneByte', onebyte)

def write_bytes_to_file(filename, bytes_to_write):
    filepath = f'TestFiles/{filename}'
    with open(filepath, 'wb+') as outputfile:
        outputfile.write(bytes_to_write)
